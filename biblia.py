import discord
import os



version = 1
autheur = "oxorothee"
contributeur = []


class Biblia(discord.Client):

    def __init__(self):
        super().__init__()
        self.help = ""
        self.list_categorie = []
        self.list_class_channel = []
        for i in self.list_class_channel:
            self.classdata = ChannellCommand(i[0],self,i[1])
            i.append(self.classdata)



    async def on_ready(self):

        print("Biblia est operationnelle")

    async def on_message(self, message):
        """def instance_channel_exist(message):
            if(message.channel.id in self.list_class_channel):
                return True

        if(not instance_channel_exist(message)):
            self.datainstance.new_instance(message.channel.id)
            self.list_class_channel.append([message.channel.id,"none","none","none"])
            self.newclass = ChannellCommand(message.channel.id,self,"none")"""


        if(message.author == self.user):
            return

        elif(message.content.startswith("biblia credit")):
            await message.channel.send(f"version = {version}")
            await message.channel.send(f"autheur de Biblia: **{autheur}**")
            if(contributeur != []):
                for i in contributeur:
                    await message.channel.send(f"contributeur au devloppement de biblia: **{i}**")

        elif(message.content.startswith("biblia help")):
            await message.channel.send("voici l'aide pour le bot biblia")

        elif(message.content.startswith("biblia admin")):

            self.classcommande = ChannellCommand(message.channel.id,self,"none")
            await self.classcommande.admin(message)




class ChannellCommand(Biblia):

    def __init__(self, channelID, instance, type):
        super().__init__()
        self.biblia = instance
        self.type = type
        self.channel = self.biblia.get_channel(channelID)
        print("class créé")



    def check_droit_admin(self, message):

        """
        cette fonction permet de definir si le membre a les droit administrateur

        :return TRUE
            si l'utilisateur a droit administrateur

        :return FALSE
            si l'utilisateur n'a pas les droit administrateur

        """
        return message.author.guild_permissions.administrator

    async def comfirmation(self, ctx, message_de_comfirmation="etes vous sur de vouloir poursuivre"):

        """


        :param ctx:
            prend le contexte en paramettre(objet de la reaction du bot)

        :param message_de_comfirmation:
            le message a afficher a l'utilisateur pour comfirmer qu'il souhaite poursuivre

        :return:
            TRUE : l'utilisateur a valider qu'il souhaite poursuivre

            FALSE : l'utilisateur ne souhaite pas continuer
        """

        def checkemoji(reaction, user):
            return self.validation_message.author != user  and self.validation_message.id == reaction.message.id and (str(reaction.emoji) == "✅" or str(reaction.emoji) == "🅾️")

        self.validation_message = await self.channel.send(f"{message_de_comfirmation}"
                                                         f"\n"
                                                         f"\n           ✅  --->  continuer"
                                                         f"\n           🅾️  --->  annuler")
        await self.validation_message.add_reaction("✅")
        await self.validation_message.add_reaction("🅾️")

        reaction, user = await self.biblia.wait_for("reaction_add", check=checkemoji)

        if (str(reaction.emoji) == "✅"):
            return True
        else:
            return False

    async def clean(self,full= False, ctx="none"):

        """
        :param ctx: prend le contexte en paramettre(objet de la reaction du bot)

        :param full:

            True: supprime tout les message du salon jusqu'a celui du contexte

            False: supprime tout les message du salon sans exeption

        """

        if(full):
            self.historique = await self.channel.history(limit=1000).flatten()
            for i in self.historique:
                await i.delete()
        else:
            self.historique = await self.channel.history(limit=1000).flatten()
            for i in self.historique:
                if (i != ctx):
                    await i.delete()
                else:
                    await i.delete()
                    break
        return True

    async def admin(self, ctx):

        def checkemoji(reaction, user):
            return self.biblireact.author != user and self.check_droit_admin(ctx) and self.biblireact.id == reaction.message.id and ( str(reaction.emoji) == "3️⃣" or str(reaction.emoji) == '2️⃣' or str(reaction.emoji) == '1️⃣' or str(reaction.emoji) == '❌')

        if(not self.check_droit_admin(ctx)):
            return

        self.biblireact = await self.channel.send("-                                                                                         Moderation de Biblia"
                                                     "\n"
                                                     "\n**COMMANDES**"
                                                     "\n"
                                                     "\n"
                                                     "\n            1️⃣  --->   transformer ce salon"
                                                     "\n            2️⃣  --->   ajouter ce salon au categorie pour indexer les ouvrage"
                                                     "\n            3️⃣  --->   supprimer tout les message du salon"
                                                     "\n            ❌  --->   quitter le mode moderation")
        await self.biblireact.add_reaction("1️⃣")
        await self.biblireact.add_reaction("2️⃣")
        await self.biblireact.add_reaction("3️⃣")
        await self.biblireact.add_reaction("❌")

        self.reaction, user = await self.biblia.wait_for(event="reaction_add", check=checkemoji)
        print("test")


        await self.biblireact.delete()

        if(self.reaction.emoji == "1️⃣"):        #------------------------------------------transforme salon------------------------------------------
            self.biblireact = await self.channel.send("vous souhaiter transformer ce salon en salon **d'indexage** :"
                                                                  "\n       - de preference reserver au moderateur/administrateur"
                                                                  "\n       - le salon d'indexage a entrer les ouvrage dans la database"
                                                                  "\n       - les autres messages seront supprimer"
                                                                  "\n"
                                                                  "\n"
                                                                  "\n       1️⃣  --->  pour selectionner le salon d'indexage (moderateur)"
                                                                  "\n"
                                                                  "\n"
                                                                  "\nvous souhaiter transformer ce salon en salon dedier a **l'index des ouvrage** :"
                                                                  "\n       - pour sont bon fonctionnement, supprimer le droit d'ecrire dans ce salon"
                                                                  "\n       - ouvert a tous, ce salon type de salon permet de consulter la database des ouvrage referencer"
                                                                  "\n       - le bot gerera totalement ce salon, les interaction ce feron part les emoji reaction"
                                                                  "\n"
                                                                  "\n"
                                                                  "\n       2️⃣  --->  pour selectionner salon d'index (public)"
                                                                  "\n"
                                                                  "\n"
                                                                  "\n       ❌  --->   annuler")

            await self.biblireact.add_reaction("1️⃣")
            await self.biblireact.add_reaction("2️⃣")
            await self.biblireact.add_reaction("❌")

            self.reaction2, user = await self.biblia.wait_for("reaction_add", check=checkemoji)
            await self.biblireact.delete()
            if(str(self.reaction2.emoji) == "1️⃣"):
                self.mess1 = "vous vous appreter a supprimer tous le contenue de ce salon pour le transformer en salon dedier a **L'indexage**" \
                             "\nce salon ne pour plus servir cas indexer des livre apres cela, les autres message seront supprimer" \
                             "\n" \
                             "\n               **etes vous sur de vouloir continuer ???**"
                if(await self.comfirmation(self.channel,message_de_comfirmation=self.mess1)):
                    if(await self.clean(ctx=ctx)):
                        await self.indexage(self.channel)
            elif(str(self.reaction2.emoji) == "2️⃣"):
                self.mess1 = "vous vous appreter a supprimer tous le contenue de ce salon pour le transformer en salon dedier a **L'index**" \
                             "\nce salon ne pour plus servir cas consulter l'index des livre apres cela" \
                             "\n" \
                             "\n               **etes vous sur de vouloir continuer ???**"
                if (await self.comfirmation(self.channel, message_de_comfirmation=self.mess1)):
                    if (await self.clean(ctx=ctx)):
                        await self.index(self.channel)
            else:
                await self.clean(self.channel,ctx=ctx)

        elif(self.reaction.emoji == "2️⃣"):      #------------------------------------------salon dedier a une categorie------------------------------------------
            await self.channel.send("maleureusement, cette partie la n'est pas encore disponnible"
                                   "\noffre un caffée a ce pauvre devloppeur pour le motiver")

        elif (self.reaction.emoji == "3️⃣"):     #------------------------------------------clear le salon------------------------------------------
            await self.clean(full=True)

        elif(self.reaction.emoji == "❌"):        #------------------------------------------annuler------------------------------------------
            await self.clean(ctx=ctx)


    async def indexage(self, channel):

        def check(message):
            return message.channel == self.channel

        def checkemoji1(reaction, user):
            return self.react_index.author != user and self.react_index.id == reaction.message.id and (
                        str(reaction.emoji) == '📓' or str(reaction.emoji) == '📙')



        def checkemoji3(reaction, user):
            emoji = ["1️⃣","2️⃣","3️⃣","4️⃣","5️⃣","6️⃣","7️⃣","8️⃣"]
            goodemoji = False
            for i in emoji:
                if(str(reaction.emoji) == i):
                    goodemoji = True
            return self.react_index.author != user and self.react_index.id == reaction.message.id and goodemoji

        def checkemoji4(reaction, user):
            emoji = ["1️⃣","2️⃣","3️⃣","4️⃣","5️⃣","6️⃣","7️⃣","8️⃣","✅"]
            goodemoji = False
            for i in emoji:
                if(str(reaction.emoji) == i):
                    goodemoji = True
            return self.react_index.author != user and self.react_index.id == reaction.message.id and goodemoji

        def creat_embed(title, description, annee, autheur, categorie, image, portrait_autheur,propose):
            embed = discord.Embed()
            if(title != "none" and categorie != "none"):
                embed.title = f"{categorie} - {title}"
            else:
                embed.title = title

            embed.color = 15105570
            if(image != "none"):
                embed.set_image(url=image)
            if(portrait_autheur != "none"):
                embed.set_thumbnail(url=portrait_autheur)
            if(description == "none"):
                description = "pas de description"
            if(annee != "none" and autheur != "none" and propose != "none"):
                embed.description = f"{description}" \
                                    f"\n " \
                                    f"\n---------------------------------------------- {annee} - {autheur} --" \
                                    f"\n" \
                                    f"\n proposer part {propose}"
            elif(annee != "none" and autheur != "none"):
                embed.description = f"{description}\n \n---------------------------------------------- {annee} - {autheur} --"

            elif(autheur != "none"):
                embed.description = f"{description}\n \n-------------------------------------------------------- {autheur} --"
            else:
                embed.description = description

            return embed


        self.controle_indexage = True

        while(self.controle_indexage):
            if(await self.clean(full=True)):

                self.react_index = await self.channel.send("voulez vous indexer un ouvrage ou modifier un ouvrage deja indexer"
                                        "\n"
                                        "\n"
                                        "\n         📓  --->  indexer un nouvelle ouvrage"
                                        "\n"
                                        "\n         📙  --->  modifier l'index d'un ouvrage existant")

                await self.react_index.add_reaction("📓")
                await self.react_index.add_reaction("📙")

                self.reaction, user = await self.biblia.wait_for("reaction_add", check=checkemoji1)

                await  self.react_index.delete()

                if(str(self.reaction.emoji) == "📓"):
                    self.titre = "none"
                    self.description = "none"
                    self.date = "none"
                    self.autheur = "none"
                    self.propose = "none"
                    self.portrait_autheur = "none"
                    self.image = "none"
                    self.categorie = "none"

                    while(True):
                        if(await self.clean(full=True)):
                            em = creat_embed(title=self.titre,description=self.description,annee=self.date,autheur=self.autheur,categorie=self.categorie,
                                             image=self.image, portrait_autheur=self.portrait_autheur,propose=self.propose)
                            await self.channel.send(embed=em)
                            if(self.titre == "none" or self.autheur == "none" or self.date == "none" or self.propose == "none"):
                                self.react_index = await self.channel.send("qu'elle information voulez vous rentrez ?"
                                                        "\n"
                                                        "\n titre/auteur/année/proposer part sont obligatoire"
                                                        "\n"
                                                        "\n"
                                                        "\n         1️⃣  ---> titre"
                                                        "\n         2️⃣  ---> description"
                                                        "\n         3️⃣  ---> date de publication"
                                                        "\n         4️⃣  ---> autheur"
                                                        "\n         5️⃣  ---> categorie"
                                                        "\n         6️⃣  ---> immage: face de l'ouvrage"
                                                        "\n         7️⃣  ---> immage: portrait de l'autheur"
                                                        "\n         8️⃣  ---> proposer part")

                                await self.react_index.add_reaction("1️⃣")
                                await self.react_index.add_reaction("2️⃣")
                                await self.react_index.add_reaction("3️⃣")
                                await self.react_index.add_reaction("4️⃣")
                                await self.react_index.add_reaction("5️⃣")
                                await self.react_index.add_reaction("6️⃣")
                                await self.react_index.add_reaction("7️⃣")
                                await self.react_index.add_reaction("8️⃣")

                                self.reaction, user = await self.biblia.wait_for("reaction_add", check=checkemoji3)
                                await self.react_index.delete()

                            else:
                                self.react_index = await self.channel.send("qu'elle information voulez vous rentrez ?"
                                                        "\n"
                                                        "\n"
                                                        "\n         1️⃣  ---> titre"
                                                        "\n         2️⃣  ---> description"
                                                        "\n         3️⃣  ---> date de publication"
                                                        "\n         4️⃣  ---> autheur"
                                                        "\n         5️⃣  ---> categorie"
                                                        "\n         6️⃣  ---> immage: face de l'ouvrage"
                                                        "\n         7️⃣  ---> immage: portrait de l'autheur"
                                                        "\n         8️⃣  ---> proposer part"
                                                        "\n" 
                                                        "\n         ✅  ---> valider")

                                await self.react_index.add_reaction("1️⃣")
                                await self.react_index.add_reaction("2️⃣")
                                await self.react_index.add_reaction("3️⃣")
                                await self.react_index.add_reaction("4️⃣")
                                await self.react_index.add_reaction("5️⃣")
                                await self.react_index.add_reaction("6️⃣")
                                await self.react_index.add_reaction("7️⃣")
                                await self.react_index.add_reaction("8️⃣")
                                await self.react_index.add_reaction("✅")

                                self.reaction, user = await self.biblia.wait_for("reaction_add", check=checkemoji4)
                                await self.react_index.delete()

                            if(str(self.reaction.emoji) == "✅"):
                                break

                            elif(str(self.reaction.emoji) == "1️⃣"): # titre

                                self.react_index = await self.channel.send("entréé le titre de l'ouvrage:")

                                self.reaction = await self.biblia.wait_for("message", check=check)

                                self.titre = self.reaction.content

                            elif (str(self.reaction.emoji) == "2️⃣"): # description

                                self.react_index = await self.channel.send("entréé la description de l'ouvrage:")

                                self.reaction = await self.biblia.wait_for("message", check=check)

                                self.description = self.reaction.content

                            elif (str(self.reaction.emoji) == "3️⃣"): # date de publication

                                self.react_index = await self.channel.send("entréé la date de l'ouvrage:")

                                self.reaction = await self.biblia.wait_for("message", check=check)

                                self.date = self.reaction.content

                            elif (str(self.reaction.emoji) == "4️⃣"): # autheur

                                self.react_index = await self.channel.send("entréé l'auteur' de l'ouvrage:")

                                self.reaction = await self.biblia.wait_for("message", check=check)

                                self.autheur = self.reaction.content

                            elif (str(self.reaction.emoji) == "5️⃣"): # categorie
                                pass

                            elif (str(self.reaction.emoji) == "6️⃣"): # image face de l'image

                                self.react_index = await self.channel.send("entréé l'url de l'image de la face de l'ouvrage:"
                                                                           "\n"
                                                                           "\n copier l'adresse de l'image")

                                self.reaction = await self.biblia.wait_for("message", check=check)

                                self.image = self.reaction.content

                            elif (str(self.reaction.emoji) == "7️⃣"): # portrait de l'autheur

                                self.react_index = await self.channel.send(
                                    "entréé l'url de l'image de l'autheur' de l'ouvrage:"
                                    "\n"
                                    "\n copier l'adresse de l'image")

                                self.reaction = await self.biblia.wait_for("message", check=check)

                                self.portrait_autheur = self.reaction.content

                            elif (str(self.reaction.emoji) == "8️⃣"): # proposer part

                                self.react_index = await self.channel.send("entréé celui qui propose l'ouvrage:")

                                self.reaction = await self.biblia.wait_for("message", check=check)

                                self.propose = self.reaction.content



                else: # modifier un ouvrage
                    pass

    async def index(self, channel):
        pass

















