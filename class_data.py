import pandas as pd
import os

from api import constant


class DataBase:

    def __init__(self):
        pass








class DataInstance:

    def __init__(self):
        self.chemin_data = os.path.join(constant.dossier_data,"instance_channel.csv")

    def recupe_data_liste(self):
        self.data = self._recup_data_instance()
        if(self.data != []):
            self.data = self.data.tolist()
        print(self.data)
        return self.data

    def _recup_data_instance(self):
        if(not os.path.exists(self.chemin_data)):
            return []
        else:
            self.data = pd.read_csv(self.chemin_data)
            return self.data

    def create_data_instance(self,channelid,type="none",emoji="none",categorie="none"):
        if (not os.path.exists(self.chemin_data)):
            self.data = [channelid,type,emoji,categorie]
            self.data = pd.DataFrame(data=self.data,columns=["ChannelID","type","emoji","categorie"])
            self._save_data(self.data)

    def new_instance(self,channelID,type = "none",emoji = "none",categorie = "none"):
        self.data = self._recup_data_instance()
        if(self.data != []):
            self.add = [channelID,type,emoji,categorie]
            self.add = pd.DataFrame(data=self.add,columns=["ChannelID","type","emoji","categorie"])
            self.data.append(self.add, ignore_index=True)
            self._save_data(self.data)
        else:
            self.data = [channelID,type,emoji,categorie]
            self.data = pd.DataFrame(data=self.data,columns=["ChannelID","type","emoji","categorie"])
            self._save_data(self.data)

    def modify_data_instance(self,channelid, type, emoji="none", categorie="none"):
        self.data = self._recup_data_instance()
        if(self.data != []):
            for self.label, self.row in self.data.iterrows():
                if(self.row["channelID"] == channelid):
                    self.data.iloc[self.row, "type"] = type
                    self.data.iloc[self.row, "emoji"] = emoji
                    self.data.iloc[self.row, "categorie"] = categorie
                    self._save_data(self.data)
                    return

    def retourne_liste_categorie(self):
        self.data = self._recup_data_instance()
        self.data = self.data.tolist()
        self.list = []
        for i in self.data:
            if(i[1] == "categorie"):
                self.list.append([i[2], i[3]])

        return self.list

    def _save_data(self,data):
        self.boucle = 0
        while(self.boucle < 100):
            try:
                data.to_csv(self.chemin_data)
                break
            except:
                pass


